#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  3 19:22:23 2019

@author: bkotzen
"""

"""
In this file, we analyze the VoteView data to begin to explor cooperation dynamics.
First, we process the data and get it into a workable format.
Second, we explore various variable combinations and plot the final cooperative 
    statistic as a function of the initial state of the congress in these 
    variables to try to visually see the dynamics of the system
Third, we approximate the transition matrix M that governs cooperation dynamics.
Fourth, we see which combinations of Axelrod strategies best approximate this M.
Fifth, we build M analytically to solve for the dominant eigenvectors.
"""

## ----------------------------------------------------------------------------
import sys
import os
import time
import pandas as pd
import random
import math
import json
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse
import numpy as np
## ----------------------------------------------------------------------------
## Begin define functions
def extract_information_from_rollcall(var, chamber, rollnumber):
    dftemp = name2df['rollcalls'].loc[name2df['rollcalls']['rollnumber'] == rollnumber]
    dffinal = dftemp.loc[dftemp['chamber']==chamber]
    return dffinal[var].values[0]

def create_cast_code_df(chamber, rollnumber):
    cast_code_df = pd.DataFrame()
    # Get only the members who are in this chamber
    this_chamber_df = name2df['members'].loc[name2df['members']['chamber'] == chamber]
    cast_code_df['icpsr'] = this_chamber_df['icpsr']
    cast_code_df['nominate_dim1'] = this_chamber_df['nominate_dim1']
    cast_code_df['nominate_dim2'] = this_chamber_df['nominate_dim2']
    # Now, given icpsr and chamber and rollnumber, find how this person voted
    cclist = list()
    for member in list(cast_code_df['icpsr']):
        try:
            dftemp1 = name2df['votes'].loc[name2df['votes']['icpsr'] == member]
            dftemp2 = dftemp1.loc[dftemp1['rollnumber'] == rollnumber]
            dftemp3 = dftemp2.loc[dftemp2['chamber'] == chamber]
            cast_code = dftemp3['cast_code'].values[0]
        except:
            cast_code = 'did not vote'
        cclist.append(cast_code)
    cast_code_df['cast_code'] = cclist
    
    return cast_code_df

def random_check_data_for_animation(time_for_check):
    start_time = time.time()
    results_checked = 0
    while time.time() - start_time < time_for_check:
        # sample a random bill
        billID = random.sample(list(data_for_animation),1)
        billID = billID[0]  # don't want a list
        # extract the chamber and rollnumber from ID
        billCHAMBER = [letter for letter in billID if letter.isalpha()]
        billCHAMBER = ''.join(billCHAMBER)
        billROLLNUM = [number for number in billID if number.isnumeric()]
        billROLLNUM = int(''.join(billROLLNUM))
        # get data on this bill
        sample_bill_data = data_for_animation[billID]
        billRESULT = sample_bill_data['vote_result']
        billNOM1 = sample_bill_data['nominate_mid_1']
        billNOM2 = sample_bill_data['nominate_mid_2']
        # how did a random voter act on this bill?
        voted = 'no'  # did this voter vote on the bill in question?
        while voted =='no':    
            billRandVoterICPSR = random.sample(list(sample_bill_data['voting_landscape']['icpsr']),1)
            billRandVoterICPSR = billRandVoterICPSR[0]  # don't want a list
            rand_voter_data = sample_bill_data['voting_landscape'].loc[sample_bill_data['voting_landscape']['icpsr'] == billRandVoterICPSR]
            billRandVoterNOM1 = rand_voter_data['nominate_dim1'].values[0]
            billRandVoterNOM2 = rand_voter_data['nominate_dim2'].values[0]
            billRandVoterCASTCODE = rand_voter_data['cast_code'].values[0]
            if billRandVoterCASTCODE != 'did not vote':
                voted = 'yes'
        # Begin the check:
        check_df_bill = name2df['rollcalls'].loc[name2df['rollcalls']['rollnumber'] == billROLLNUM]
        check_df_bill = check_df_bill.loc[check_df_bill['chamber'] == billCHAMBER]
        check_df_icpsr = name2df['members'].loc[name2df['members']['icpsr'] == billRandVoterICPSR]
        check_df_castcode = name2df['votes'].loc[name2df['votes']['icpsr'] == billRandVoterICPSR]
        check_df_castcode = check_df_castcode.loc[check_df_castcode['rollnumber'] == billROLLNUM]
        check_df_castcode = check_df_castcode.loc[check_df_castcode['chamber'] == billCHAMBER]
        
        if check_df_bill.empty:
            print('Error: Chamber {} and Roll Number {} do not agree.'.format(billCHAMBER, billROLLNUM))
            break
        elif billRESULT != check_df_bill['vote_result'].values[0]:
            if math.isnan(billRESULT) and math.isnan(check_df_bill['vote_result'].values[0]):
                print('No result for bill {}'.format(billID))
            else:
                print('Error: vote result incongruity for {}'.format(billID))
                break
        elif round(billNOM1,3) != round(check_df_bill['nominate_mid_1'].values[0],3):
            if math.isnan(billNOM1) and math.isnan(check_df_bill['nominate_mid_1'].values[0]):
                print('No nominate data for bill {}'.format(billID))
            else:
                print('Error: nominate incongruity for bill {}'.format(billID))
                break
        elif round(billNOM2,3) != round(check_df_bill['nominate_mid_2'].values[0],3):
            if math.isnan(billNOM2) and math.isnan(check_df_bill['nominate_mid_2'].values[0]):
                print('No nominate data for bill {}'.format(billID))
            else:
                print('Error: nominate incongruity for bill {}'.format(billID))
                break
        elif check_df_icpsr.empty:
            print('Error: congress(wo)man {} not found.'.format(billRandVoterICPSR))
            break
        elif round(billRandVoterNOM1,3) != round(check_df_icpsr['nominate_dim1'].values[0],3):
            if math.isnan(billRandVoterNOM1) and math.isnan(check_df_icpsr['nominate_dim1'].values[0]):
                print('No nominate data for congressperson {}'.format(billRandVoterICPSR))
            else:
                print('Error: nominate incongruity for congress(wo)man {}'.format(billRandVoterICPSR))
                break
        elif round(billRandVoterNOM2,3) != round(check_df_icpsr['nominate_dim2'].values[0],3):
            if math.isnan(billRandVoterNOM2) and math.isnan(check_df_icpsr['nominate_dim2'].values[0]):
                print('No nominate data for congress(wo)man {}'.format(billRandVoterICPSR))
            else:
                print('Error: nominate incongruity for congressperson {}'.format(billRandVoterICPSR))
                break
        elif check_df_castcode.empty:
            print('Error: it appears congressperson {} did not vote on bill {}'.format(billRandVoterICPSR, billID))
            break
        elif billRandVoterCASTCODE != check_df_castcode['cast_code'].values[0]:
            if billRandVoterCASTCODE == 'unknown' or math.isnan(check_df_castcode['cast_code'].values[0]):
                print('Something is funny about the cast code by {} on bill {}'.format(billRandVoterICPSR, billID))
            else:
                print('Error: cast code incongruity for congress(wo)man {} on bill {}'.format(billRandVoterICPSR, billID))
                break
        else:
            results_checked += 1
        
    print('A random sample of {} results were verified'.format(results_checked))
    
    
## End define functions
## ----------------------------------------------------------------------------



## ----------------------------------------------------------------------------
## Process the data
 # First, read the data
name2df = {}
inp = input('Data for the house? ')
if inp in ['yes', 'Yes', 'y', 'Y']:
    house = True
else:
    house = False
inp = input('Data for the senate? ')
if inp in ['yes', 'Yes', 'y', 'Y']:
    senate = True  
else:
    senate = False
congress = int(input('Congress number: '))
if house and senate:
    prefix = 'HS{}_'.format(congress)
elif house:
    prefix = 'H{}_'.format(congress)
elif senate:
    prefix = 'S{}_'.format(congress)
    
roots = ['members','parties','rollcalls','votes']

name2df = {}
try:
    suffix = '.csv'   # if we change this, we have to figure out how to redo cached_DFA_filename
    for root in roots:
        vvfile = prefix+root+suffix
        address = 'VoteView/'+vvfile
        x=pd.read_csv(address)
        name2df[root] = x
except:
    print('{} is a bad file'.format(vvfile))

 # Now, create data structure as described in OverLeaf Notes from Oct 21
 # https://www.overleaf.com/project/5d6734801722f636df5c9ebc

 # Get rollcall vote identifiers
    # since rollnumbers in house and senate are independent, there may be 
# duplicate numbers refering to different votes

chamber = tuple(name2df['rollcalls']['chamber'])
rollnumbers = tuple(name2df['rollcalls']['rollnumber'])

 # Is data already cached?
try:
    start_time_to_uncache = time.time()
    data_for_animation = dict()
    directory = prefix+'Cache'
    # Must import dictionary (without data frame key-value pair) and
    # dataframe key-value pair separately, since json wants to treat this as a nested dictionary
    for idpair in zip(chamber,rollnumbers):
        this_vote = '{}{}'.format(idpair[0],idpair[1])  # e.g. House113 for vote 113 in the House
        cached_DFA_filename = prefix+this_vote   # cached data_for_animation filename is 'HS116_votes.csv' without 'votes.csv' 
                # Ex: "HS116_House11'
                
        dictfile = cached_DFA_filename + 'dict.txt'
        address = directory+'/'+dictfile
        with open(address,'r') as file:
            r = file.read()
            cached_dict = json.loads(r)
        dffile = cached_DFA_filename + 'df.txt'
        address = directory+'/'+dffile
        with open(address,'r') as file:
            r = file.read()
            cached_df = pd.DataFrame(data=json.loads(r))
        data_for_animation[this_vote] = cached_dict
        data_for_animation[this_vote]['voting_landscape'] = cached_df
        
except FileNotFoundError:
    start_time_to_cache = time.time()
    # If data is not cached... 
    print('Data not cached for {}'.format(prefix[:-1]))
    
    data_for_animation = dict()
    # For the cache...
    directory = prefix+'Cache'
    cwd = os.getcwd()
    if not os.path.exists(cwd+'/'+directory):
        os.mkdir(directory)
    
    for idpair in zip(chamber,rollnumbers):
        this_vote = '{}{}'.format(idpair[0],idpair[1])  # e.g. House113 for vote 113 in the House
        metadata_dict = dict()  # a dictionary for storing metadata and a big member ideology data frame
        metadata_dict['description'] = extract_information_from_rollcall('vote_desc', idpair[0], idpair[1])
        metadata_dict['vote_result'] = extract_information_from_rollcall('vote_result', idpair[0], idpair[1])  
        metadata_dict['nominate_mid_1'] = extract_information_from_rollcall('nominate_mid_1', idpair[0], idpair[1])
        metadata_dict['nominate_mid_2'] = extract_information_from_rollcall('nominate_mid_2', idpair[0], idpair[1])
        metadata_dict['nominate_spread_1'] = extract_information_from_rollcall('nominate_spread_1', idpair[0], idpair[1])
        metadata_dict['nominate_spread_2'] = extract_information_from_rollcall('nominate_spread_2', idpair[0], idpair[1])
        metadata_dict['voting_landscape'] = create_cast_code_df(idpair[0], idpair[1])
        
        print('Amount of metadata for vote {} in the {}: {} bytes'.format(idpair[1], idpair[0], sys.getsizeof(metadata_dict)))
        
        data_for_animation[this_vote] = metadata_dict
    
         # Now we cache the data
        cached_DFA_filename = prefix+this_vote   # cached data_for_animation filename is 'HS116_votes.csv' withoout 'votes.csv' 
                # Ex: "HS116_House11'       
        cache_df = data_for_animation[this_vote]['voting_landscape']
        cache_dict = data_for_animation[this_vote]  # need to pass by value, not reference
        x = cache_dict['voting_landscape']
        del cache_dict['voting_landscape']
        
        dictfile = cached_DFA_filename + 'dict.txt'
        address = directory+'/'+dictfile
        with open(address,'w') as file:
            file.write(json.dumps(cache_dict))
        dffile = cached_DFA_filename + 'df.txt'
        address = directory+'/'+dffile
        with open(address,'w') as file:
            json_cache_df = cache_df.to_json(orient=None)
            file.write(json_cache_df)
        
        cache_dict['voting_landscape']=x  # we need to replace the values we deleted
                                        # since python is pass-by-reference not pass-by-value
                                        # and so deleting voting_landscape in the cache deletes
                                        # it in the original as well
    
end_time = time.time()    
print('Amount of data collected for congress {}: {} bytes'.format(congress, sys.getsizeof(data_for_animation)))

 # Randomly check the data for accuracy
time_for_check = .1  # how many seconds should we check for?
random_check_data_for_animation(time_for_check)
    


## End data processing
## ----------------------------------------------------------------------------



## ----------------------------------------------------------------------------
## Explore basins of cooperation
    








