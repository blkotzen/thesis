#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  3 19:22:23 2019

@author: bkotzen
"""

"""
In this file, we analyze the VoteView data to begin to explor cooperation dynamics.
First, we process the data and get it into a workable format.
Second, we explore various variable combinations and plot the final cooperative 
    statistic as a function of the initial state of the congress in these 
    variables to try to visually see the dynamics of the system
Third, we approximate the transition matrix M that governs cooperation dynamics.
Fourth, we see which combinations of Axelrod strategies best approximate this M.
Fifth, we build M analytically to solve for the dominant eigenvectors.
"""

## ----------------------------------------------------------------------------
import sys
import os
import time
import pandas as pd
import random
import math
import json
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Ellipse
import numpy as np
## ----------------------------------------------------------------------------
## Begin define functions
def extract_information_from_ith_rollcall(var, chamber, rollnumber):
    dftemp = ithname2df['rollcalls'].loc[ithname2df['rollcalls']['rollnumber'] == rollnumber]
    dffinal = dftemp.loc[dftemp['chamber']==chamber]
    return dffinal[var].values[0]

def create_ith_cast_code_df(chamber, rollnumber):
    cast_code_df = pd.DataFrame()
    # Get only the members who are in this chamber
    this_chamber_df = ithname2df['members'].loc[name2df['members']['chamber'] == chamber]
    cast_code_df['icpsr'] = this_chamber_df['icpsr']
    cast_code_df['nominate_dim1'] = this_chamber_df['nominate_dim1']
    cast_code_df['nominate_dim2'] = this_chamber_df['nominate_dim2']
    # Now, given icpsr and chamber and rollnumber, find how this person voted
    cclist = list()
    for member in list(cast_code_df['icpsr']):
        try:
            dftemp1 = ithname2df['votes'].loc[name2df['votes']['icpsr'] == member]
            dftemp2 = dftemp1.loc[dftemp1['rollnumber'] == rollnumber]
            dftemp3 = dftemp2.loc[dftemp2['chamber'] == chamber]
            cast_code = dftemp3['cast_code'].values[0]
        except:
            cast_code = 'did not vote'
        cclist.append(cast_code)
    cast_code_df['cast_code'] = cclist
    
    return cast_code_df

def ithrandom_check_data_for_animation(time_for_check):
    start_time = time.time()
    results_checked = 0
    while time.time() - start_time < time_for_check:
        # sample a random bill
        billID = random.sample(list(ithdata_for_animation),1)
        billID = billID[0]  # don't want a list
        # extract the chamber and rollnumber from ID
        billCHAMBER = [letter for letter in billID if letter.isalpha()]
        billCHAMBER = ''.join(billCHAMBER)
        billROLLNUM = [number for number in billID if number.isnumeric()]
        billROLLNUM = int(''.join(billROLLNUM))
        # get data on this bill
        sample_bill_data = ithdata_for_animation[billID]
        billRESULT = sample_bill_data['vote_result']
        billNOM1 = sample_bill_data['nominate_mid_1']
        billNOM2 = sample_bill_data['nominate_mid_2']
        # how did a random voter act on this bill?
        voted = 'no'  # did this voter vote on the bill in question?
        while voted =='no':    
            billRandVoterICPSR = random.sample(list(sample_bill_data['voting_landscape']['icpsr']),1)
            billRandVoterICPSR = billRandVoterICPSR[0]  # don't want a list
            rand_voter_data = sample_bill_data['voting_landscape'].loc[sample_bill_data['voting_landscape']['icpsr'] == billRandVoterICPSR]
            billRandVoterNOM1 = rand_voter_data['nominate_dim1'].values[0]
            billRandVoterNOM2 = rand_voter_data['nominate_dim2'].values[0]
            billRandVoterCASTCODE = rand_voter_data['cast_code'].values[0]
            if billRandVoterCASTCODE != 'did not vote':
                voted = 'yes'
        # Begin the check:
        check_df_bill = ithname2df['rollcalls'].loc[ithname2df['rollcalls']['rollnumber'] == billROLLNUM]
        check_df_bill = check_df_bill.loc[check_df_bill['chamber'] == billCHAMBER]
        check_df_icpsr = ithname2df['members'].loc[ithname2df['members']['icpsr'] == billRandVoterICPSR]
        check_df_castcode = ithname2df['votes'].loc[ithname2df['votes']['icpsr'] == billRandVoterICPSR]
        check_df_castcode = check_df_castcode.loc[check_df_castcode['rollnumber'] == billROLLNUM]
        check_df_castcode = check_df_castcode.loc[check_df_castcode['chamber'] == billCHAMBER]
        
        if check_df_bill.empty:
            print('Error: Chamber {} and Roll Number {} do not agree.'.format(billCHAMBER, billROLLNUM))
            break
        elif billRESULT != check_df_bill['vote_result'].values[0]:
            if math.isnan(billRESULT) and math.isnan(check_df_bill['vote_result'].values[0]):
                print('No result for bill {}'.format(billID))
            else:
                print('Error: vote result incongruity for {}'.format(billID))
                break
        elif round(billNOM1,3) != round(check_df_bill['nominate_mid_1'].values[0],3):
            if math.isnan(billNOM1) and math.isnan(check_df_bill['nominate_mid_1'].values[0]):
                print('No nominate data for bill {}'.format(billID))
            else:
                print('Error: nominate incongruity for bill {}'.format(billID))
                break
        elif round(billNOM2,3) != round(check_df_bill['nominate_mid_2'].values[0],3):
            if math.isnan(billNOM2) and math.isnan(check_df_bill['nominate_mid_2'].values[0]):
                print('No nominate data for bill {}'.format(billID))
            else:
                print('Error: nominate incongruity for bill {}'.format(billID))
                break
        elif check_df_icpsr.empty:
            print('Error: congress(wo)man {} not found.'.format(billRandVoterICPSR))
            break
        elif round(billRandVoterNOM1,3) != round(check_df_icpsr['nominate_dim1'].values[0],3):
            if math.isnan(billRandVoterNOM1) and math.isnan(check_df_icpsr['nominate_dim1'].values[0]):
                print('No nominate data for congressperson {}'.format(billRandVoterICPSR))
            else:
                print('Error: nominate incongruity for congress(wo)man {}'.format(billRandVoterICPSR))
                break
        elif round(billRandVoterNOM2,3) != round(check_df_icpsr['nominate_dim2'].values[0],3):
            if math.isnan(billRandVoterNOM2) and math.isnan(check_df_icpsr['nominate_dim2'].values[0]):
                print('No nominate data for congress(wo)man {}'.format(billRandVoterICPSR))
            else:
                print('Error: nominate incongruity for congressperson {}'.format(billRandVoterICPSR))
                break
        elif check_df_castcode.empty:
            print('Error: it appears congressperson {} did not vote on bill {}'.format(billRandVoterICPSR, billID))
            break
        elif billRandVoterCASTCODE != check_df_castcode['cast_code'].values[0]:
            if billRandVoterCASTCODE == 'unknown' or math.isnan(check_df_castcode['cast_code'].values[0]):
                print('Something is funny about the cast code by {} on bill {}'.format(billRandVoterICPSR, billID))
            else:
                print('Error: cast code incongruity for congress(wo)man {} on bill {}'.format(billRandVoterICPSR, billID))
                break
        else:
            results_checked += 1
        
    print('A random sample of {} results were verified'.format(results_checked))
    
    
## End define functions
## ----------------------------------------------------------------------------



## ----------------------------------------------------------------------------
## Process the data
 # First, read the data
name2df = {}
prefix = 'HSall_'    
roots = ['members','parties','rollcalls','votes']
try:
    suffix = '.csv'   # if we change this, we have to figure out how to redo cached_DFA_filename
    for root in roots:
        vvfile = prefix+root+suffix
        address = 'VoteView/'+vvfile
        x=pd.read_csv(address)
        name2df[root] = x
except:
    print('{} is a bad file'.format(vvfile))
    
    
congress2DFA = dict()
minCongress = int(input('Min congress? ')) -1
maxCongress = int(input('Max Congress? '))
for congress in range(minCongress, maxCongress):
    congress += 1
    for chamber in ('House', 'Senate'):
        if chamber == 'House':
            prefix = 'H{}_'.format(congress)
        else:
            prefix = 'S{}_'.format(congress)
        ithname2df = dict()
        for root in roots:
            # Copy data from the massive dictionary into this mini-dictionary for one chamber, one session
            ithname2df[root] =    name2df[root].loc[   name2df[root]['congress']==congress]
            ithname2df[root] = ithname2df[root].loc[ithname2df[root]['chamber'] ==chamber]
            
            
        try:
            # Cached?
            ithdata_for_animation = dict()
            directory = prefix+'Cache'
            
            for rollnumber in ithname2df['rollcalls']['rollnumber']:
                this_vote = '{}{}'.format(chamber,rollnumber)  # e.g. House113 for vote 113 in the House
                cached_DFA_filename = prefix+this_vote   # cached data_for_animation filename is 'HS116_votes.csv' without 'votes.csv' 
                        # Ex: "H12_House11
                        
                dictfile = cached_DFA_filename + 'dict.txt'
                address = 'cache/'+directory+'/'+dictfile
                with open(address,'r') as file:
                    r = file.read()
                    cached_dict = json.loads(r)
                dffile = cached_DFA_filename + 'df.txt'
                address = 'cache/'+directory+'/'+dffile
                with open(address,'r') as file:
                    r = file.read()
                    cached_df = pd.DataFrame(data=json.loads(r))
                ithdata_for_animation[this_vote] = cached_dict
                ithdata_for_animation[this_vote]['voting_landscape'] = cached_df
            
        except FileNotFoundError:
            print('Caching data for {}{}'.format(chamber,congress))
            ithdata_for_animation = dict()
            
            directory = prefix+'Cache'
            cwd = os.getcwd()
            if not os.path.exists(cwd+'/cache/'+directory):
                os.mkdir('cache/'+directory)
                
            for rollnumber in ithname2df['rollcalls']['rollnumber']:
                this_vote = '{}{}'.format(chamber,rollnumber)
                metadata_dict = dict()  # a dictionary for storing metadata and a big member ideology data frame
                metadata_dict['description'] = extract_information_from_ith_rollcall('vote_desc', chamber, rollnumber)
                metadata_dict['vote_result'] = extract_information_from_ith_rollcall('vote_result', chamber, rollnumber)  
                metadata_dict['nominate_mid_1'] = extract_information_from_ith_rollcall('nominate_mid_1', chamber, rollnumber)
                metadata_dict['nominate_mid_2'] = extract_information_from_ith_rollcall('nominate_mid_2', chamber, rollnumber)
                metadata_dict['nominate_spread_1'] = extract_information_from_ith_rollcall('nominate_spread_1', chamber, rollnumber)
                metadata_dict['nominate_spread_2'] = extract_information_from_ith_rollcall('nominate_spread_2', chamber, rollnumber)
                metadata_dict['voting_landscape'] = create_ith_cast_code_df(chamber, rollnumber)
                
                ithdata_for_animation[this_vote] = metadata_dict
                
                 # Now we cache the data
                cached_DFA_filename = prefix+this_vote   # cached ithdata_for_animation filename is 'HS116_votes.csv' withoout 'votes.csv' 
                    # Ex: "H12_House11'  
                cache_df = ithdata_for_animation[this_vote]['voting_landscape']
                cache_dict = ithdata_for_animation[this_vote]  # need to pass by value, not reference
                x = cache_dict['voting_landscape']
                del cache_dict['voting_landscape']
            
                dictfile = cached_DFA_filename + 'dict.txt'
                address = 'cache/'+directory+'/'+dictfile
                with open(address,'w') as file:
                    file.write(json.dumps(cache_dict))
                    dffile = cached_DFA_filename + 'df.txt'
                address = 'cache/'+directory+'/'+dffile
                with open(address,'w') as file:
                    json_cache_df = cache_df.to_json(orient=None)
                    file.write(json_cache_df)
            
                cache_dict['voting_landscape']=x  # we need to replace the values we deleted
                                            # since python is pass-by-reference not pass-by-value
                                            # and so deleting voting_landscape in the cache deletes
                                            # it in the original as well
             # Randomly check the data for accuracy
            time_for_check = .1  # how many seconds should we check for?
            ithrandom_check_data_for_animation(time_for_check)
            
            
        congress2DFA[congress] = ithdata_for_animation   


## End data processing
## ----------------------------------------------------------------------------



## ----------------------------------------------------------------------------
## Explore basins of cooperation
# Let's explpore properties of the first n bills
n2basindict = dict()
#startdf = pd.DataFrame({'cooperation':[],'center':[]})
#enddf   = pd.DataFrame({'cooperation':[],'center':[]})
for congress in congress2DFA.keys():
    n = 10  # how many bills do we want to explore at the beginning / end?
    count = 0
    startsizelist = []
    startcenterlist = []
    while count < n:
        for bill in list(congress2DFA[congress].keys())[0:n]:
            count += 1
            
            voter_df = congress2DFA[congress][bill]['voting_landscape']
            # Group members by vote
            yea_voters = voter_df.loc[list(voter_df['cast_code']==1) 
                                   or list(voter_df['cast_code']==2)
                                   or list(voter_df['cast_code']==3)]
            nay_voters = voter_df.loc[list(voter_df['cast_code']==4) 
                                   or list(voter_df['cast_code']==5)
                                   or list(voter_df['cast_code']==6)]
            other_voters = voter_df.loc[list(voter_df['cast_code']==0) 
                                     or list(voter_df['cast_code']==7)
                                     or list(voter_df['cast_code']==8)
                                     or list(voter_df['cast_code']==9)]
            
            num_y = len(list(yea_voters['icpsr']))
            num_n = len(list(nay_voters['icpsr']))
            num_o = len(list(other_voters['icpsr']))
            
            # What's the most agreement we see?
            most_cooperation = max(num_y, num_n, num_o)
            # What is the ideological center of the agreeing coalition
            if len(list(yea_voters['icpsr'])) == most_cooperation:
                nom_center = np.mean(list(yea_voters['nominate_dim1'].values))
            elif len(list(nay_voters['icpsr'])) == most_cooperation:
                nom_center = np.mean(list(nay_voters['nominate_dim1'].values))
            elif len(list(other_voters['icpsr'])) == most_cooperation:
                nom_center = np.mean(list(other_voters['nominate_dim1'].values))
                
            startsizelist.append(most_cooperation)
            startcenterlist.append(nom_center)
            
#            n2basindict[count]=dict()  # initialize dictionary entry
#            n2basindict[count]['starting cooperation'] = np.mean(startsizelist)
#            n2basindict[count]['starting center'     ] = np.mean(startcenterlist)
            
             # Append results from this congress to those from the previous congress
            df = pd.DataFrame({'cooperation':[np.mean(startsizelist)],'center':[np.mean(startcenterlist)]})
            try:  # see if we've already set up the data frame for this count
                startdf = n2basindict[count]['start']
                startdf = startdf.append(df)
                
            except:   # need to set up the data frame here
                startdf = df
            
             # Paste this updated df into the count entry
            try:
                n2basindict[count]['start'] = startdf
            except KeyError:
                n2basindict[count] = dict()
                n2basindict[count]['start'] = startdf
                 
                
            
            
            
            
    count = 0
    endsizelist = []
    endcenterlist = []
    while count < n:
        for bill in list(congress2DFA[congress].keys())[-n:]:
            count += 1
            
            voter_df = congress2DFA[congress][bill]['voting_landscape']
            # Group members by vote
            yea_voters = voter_df.loc[list(voter_df['cast_code']==1) 
                                   or list(voter_df['cast_code']==2)
                                   or list(voter_df['cast_code']==3)]
            nay_voters = voter_df.loc[list(voter_df['cast_code']==4) 
                                   or list(voter_df['cast_code']==5)
                                   or list(voter_df['cast_code']==6)]
            other_voters = voter_df.loc[list(voter_df['cast_code']==0) 
                                     or list(voter_df['cast_code']==7)
                                     or list(voter_df['cast_code']==8)
                                     or list(voter_df['cast_code']==9)]
            
            num_y = len(list(yea_voters['icpsr']))
            num_n = len(list(nay_voters['icpsr']))
            num_o = len(list(other_voters['icpsr']))
            
            # What's the most agreement we see?
            most_cooperation = max(num_y, num_n, num_o)
            # What is the ideological center of the agreeing coalition
            if len(list(yea_voters['icpsr'])) == most_cooperation:
                nom_center = np.mean(list(yea_voters['nominate_dim1'].values))
            elif len(list(nay_voters['icpsr'])) == most_cooperation:
                nom_center = np.mean(list(nay_voters['nominate_dim1'].values))
            elif len(list(other_voters['icpsr'])) == most_cooperation:
                nom_center = np.mean(list(other_voters['nominate_dim1'].values))
                
            endsizelist.append(most_cooperation)
            endcenterlist.append(nom_center)
            
             # don't need to initialize n2basindict[count] entry because it's already been established
             # but just in case...
#            try:
#                n2basindict[count]['ending cooperation'] = np.mean(endsizelist)
#                n2basindict[count]['ending center'     ] = np.mean(endcenterlist)
#            except:
#                n2basindict[count]=dict()
#                n2basindict[count]['starting cooperation'] = np.mean(startsizelist)
#                n2basindict[count]['starting center'     ] = np.mean(startcenterlist)

                        
             # Append results from this congress to those from the previous congress
            df = pd.DataFrame({'cooperation':[np.mean(endsizelist)],'center':[np.mean(endcenterlist)]})
            try:  # see if we've already set up the data frame for this count
                enddf = n2basindict[count]['end']
                enddf = enddf.append(df)
                
            except:   # need to set up the data frame here
                enddf = df
            
             # Paste this updated df into the count entry
            try:
                n2basindict[count]['end'] = enddf
            except KeyError:
                n2basindict[count] = dict()
                n2basindict[count]['end'] = enddf
                
            
            








