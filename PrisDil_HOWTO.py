#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 16:29:32 2019

@author: bkotzen



The Axelrod module provides tools for simulating a prisoner's dilemma on python.
It can be downloaded from github at 
        ---  https://github.com/Axelrod-Python/Axelrod  ---
Besides the readme in this git repository, there is further documentation at 
        ---  https://axelrod.readthedocs.io/en/stable/  ---
and
    --- https://openresearchsoftware.metajnl.com/article/10.5334/jors.125/ --

This code takes a look at the package and documents usage examples.

"""

import axelrod as axl
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx


##### Define functions here #####






##### End define functions #####

## Example 1: Demo
strategies = [s() for s in axl.demo_strategies]  # list strategies
tournament = axl.Tournament(strategies)  # create tournament object
   # NOTE: tournament object allows for player networks, variable number of games, etc.
results = tournament.play()  # can be parallelized, and output can be written to a file
plot = axl.Plot(results)
p = plot.boxplot()
p.show()

## Exmple 2: Demo varition with more strategies
strategies = [axl.Cooperator(),
              axl.Defector(),
              axl.ZDExtort2(),
              axl.Joss(),
              axl.HardTitForTat(),
              axl.HardTitFor2Tats(),
              axl.TitForTat(),
              axl.Grudger(),
              axl.TitFor2Tats(),
              axl.WinStayLoseShift(),
              axl.Random(),
              axl.ZDGTFT2(),
              axl.HardProber(),
              axl.Prober(),
              axl.Prober2(),
              axl.Prober3(),
              axl.Calculator(),
              axl.HardGoByMajority()]
for i in strategies:
    print('_________')
    print(i)
    print(i.__doc__)
    print()
tournament = axl.Tournament(strategies)  # create tournament object
results = tournament.play()  # can be parallelized, and output can be written to a file
plot = axl.Plot(results)
p = plot.boxplot()
p.show()

## Example 3: Demo variation with ALL strategies
# TAKES TOO LONG, BUT CAN BE DONE
#strategies = [s() for s in axl.ordinary_strategies]  # list strategies
##for i in strategies:
##    print('_________')
##    print(i)
##    print(i.__doc__)
##    print()
#tournament = axl.Tournament(strategies)  # create tournament object
#   # NOTE: tournament object allows for player networks, variable number of games, etc.
#results = tournament.play()  # can be parallelized, and output can be written to a file
#plot = axl.Plot(results)
#p = plot.boxplot()
#p.show()

## Demo 1: How to set up a match, properties, etc.
players = [axl.Prober(), axl.TitForTat()]  # can be list or tuple
match = axl.Match(players, 5, noise = 0.01)  # players switch strategies w/ prob 0.01
print(match.play())
# Visualize results:
print(match.sparklines())   # uses block for cooperation, space for defection
print(match.sparklines(c_symbol=' :-) ',d_symbol=' >:( '))    # use custom symbols
# print(match.scores()) # can see scores this way, or can present them more neatly as:
print('P1  P2')
for i in match.scores():
    print(' {}   {}'.format(i[0],i[1]))
print('Final:')
print('{}  {}'.format(match.final_score()[0], match.final_score()[1]))
print('# Cooperations:')
print(' {}   {}'.format(match.cooperation()[0],match.cooperation()[1]))
print('Percent cooperation:')
print('{:.1f}  {:.1f}'.format(match.normalised_cooperation()[0],match.normalised_cooperation()[1]))

# Demo 2: How to set up a tournament
players = [axl.Cooperator(), axl.Defector(), axl.TitForTat(), axl.Grudger()]
tournament = axl.Tournament(players, turns = 5, repetitions=1)  # set a pure round-robin tournament
                                        # Give each player t turns and repeat the tourney r times
results = tournament.play()  # play the tournament
# suppose we don't want to ever have a match between cooperator & defector
# Alter the  round-robin complete graph topology:
edges = [(0,2), (0,3), (1,2), (1,3), (2,3)]
spatial_tournament = axl.Tournament(players, turns = 2, repetitions = 1, edges=edges)  # use the specified topology
spatial_results = spatial_tournament.play()
# Noticed the differences between the complete and custom networks
print('Complete: ',results.ranked_names)
print('Custom:   ',spatial_results.ranked_names)
#Check out the payoffs. 
#The nth row is the nth player's avg. payoffs over the t turns. 
#The mth col is the mth player's opponent
#results.payoffs
#spatial_results.payoffs

# Demo 3: Morality metrics
strategies = [axl.Cooperator(), axl.Defector(), axl.ZDExtort2(), axl.Joss(),
              axl.HardTitForTat(), axl.HardTitFor2Tats(), axl.TitForTat(),
              axl.Grudger(), axl.TitFor2Tats(), axl.WinStayLoseShift(),
              axl.Random(), axl.ZDGTFT2(), axl.HardProber(), axl.Prober(), 
              axl.Prober2(), axl.Prober3(), axl.Calculator(), axl.HardGoByMajority()]
tournament = axl.Tournament(strategies)  # create tournament object
results = tournament.play()
# ---- Cooperation rating -------
labels = []  # for bar plot
for i in strategies:
    l = str(i)
    l = l.split(':')[0]
    labels.append(l)
# To sort the labels by the cooperating rating, zip the lists together
dat = zip(results.cooperating_rating, labels)
dat = sorted(dat)
x = [x[1] for x in dat]
y = [y[0] for y in dat]
fig = plt.figure()
plt.barh(x,y)
plt.xlabel('Percent moves cooperation')
fig.savefig('cooperation_example.png')
# ---- Good-Partner rating -------
dat = zip(results.good_partner_rating, labels)
dat = sorted(dat)
x = [x[1] for x in dat]
y = [y[0] for y in dat]
fig = plt.figure()
plt.barh(x,y)
plt.xlabel('Percent matches of equal or superior cooperation')
fig.savefig('goodpartner_example.png')
# ---- Good-Partner matrix -------
G = nx.Graph()
for ind1, i in enumerate(results.good_partner_matrix):
    for (ind2, j) in enumerate(i):
        p1 = 's: ' + str(strategies[ind1])  # important to distinguish sources and targets
        p2 = 't: ' + str(strategies[ind2])
        weight = j
        G.add_node(p1, bipartite=0)  # establish bipartite classes 0 and 1
        G.add_node(p2, bipartite=1)
        G.add_weighted_edges_from([(p1,p2,weight)])
source_nodes = [node for node in G.node if 's: ' in node]
edge_widths = list(float(G[s][t]['weight']) for s,t in G.edges)
width_scalar = .5/(max(edge_widths))
scaled_edge_widths = [width_scalar * el for el in edge_widths]
fig=nx.draw_networkx(G, pos = nx.drawing.layout.bipartite_layout(G, source_nodes), 
                 width=scaled_edge_widths)
fig.savefig('goodpair_example.png')
# ---- Eigenjesus rating -------
labels = []  # format the strategies list
for i in strategies:
    l = str(i)
    l = l.split(':')[0]
    labels.append(l)
datj = zip(results.eigenjesus_rating, labels)
datj = sorted(datj, reverse=True)
print('Rank  |  Eig. Jesus  |  Player')
print('---------------------------------------')
for (index, el) in enumerate(datj):
    rank = index + 1
    print('{:3.0f}      {:.2f}           {:s}'.format(rank, el[0], el[1]))
# ---- Eigenmoses rating -------
labels = []  # format the strategies list
for i in strategies:
    l = str(i)
    l = l.split(':')[0]
    labels.append(l)
datm = zip(results.eigenmoses_rating, labels)
datm = sorted(datm, reverse=True)
print('Rank  |  Eig. Jesus  |  Player')
print('---------------------------------------')
for (index, el) in enumerate(datm):
    rank = index + 1
    print('{:3.0f}      {:.2f}           {:s}'.format(rank, el[0], el[1]))
# What's the mean rank change?
rankChange = []
for (jrank, el1) in enumerate(datj):
    player1 = el1[1]
    for (mrank, el2) in enumerate(datm):
        player2 = el2[1]
        if player1 == player2:
            rankChange.append(abs(mrank-jrank))





