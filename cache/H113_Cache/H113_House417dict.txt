{"description": "To amend subtitle D of the Solid Waste Disposal Act to encourage recovery and beneficial use of coal combustion residuals and establish requirements for the proper management and disposal of coal combustion residuals that are protective of human health and the environment", "vote_result": "Passed", "nominate_mid_1": 0.188, "nominate_mid_2": -0.466, "nominate_spread_1": -0.252, "nominate_spread_2": -1.062}