{"icpsr":{"345":1914,"346":2316,"347":2355,"348":3678,"349":3846,"350":4433,"351":8671,"352":9127,"353":9452,"354":7235,"355":379,"356":6472,"357":3798,"358":7084,"359":1716,"360":2125,"361":2139,"362":2512,"363":2796,"364":4441,"365":6830,"366":8678,"367":8810,"368":8811,"369":154,"370":947,"371":2451,"372":3303,"373":3358,"374":3674,"375":5611,"376":5850,"377":5852,"378":7788,"379":8344,"380":8376,"381":8563,"382":9268,"383":9650,"384":9707,"385":3299,"386":3610,"387":8448,"388":8644,"389":2440,"390":4315,"391":5303,"392":8637,"393":9353,"394":351,"395":2058,"396":3573,"397":3628,"398":4193,"399":4204,"400":5715,"401":9591,"402":9602,"403":10159,"404":805,"405":1176,"406":1255,"407":3342,"408":3594,"409":3859,"410":4519,"411":5734,"412":5895,"413":9048,"414":9187,"415":429,"416":2881,"417":3147,"418":3437,"419":3805,"420":4156,"421":4390,"422":5310,"423":5890,"424":6781,"425":7870,"426":8555,"427":9107,"428":9308,"429":884,"430":5935,"431":7553,"432":656,"433":2816,"434":4019,"435":4090,"436":8702,"437":10275,"438":4843,"439":1195,"440":8639,"441":1007,"442":1380,"443":1739,"444":1853,"445":1936,"446":3582,"447":4026,"448":4127,"449":4269,"450":4850,"451":5903,"452":6625,"453":6886,"454":6911,"455":7156,"456":7199,"457":7603,"458":8148,"459":9658},"nominate_dim1":{"345":0.486,"346":0.776,"347":0.833,"348":0.759,"349":0.776,"350":0.691,"351":0.666,"352":0.475,"353":0.727,"354":-0.132,"355":-0.165,"356":-0.015,"357":-0.183,"358":-0.022,"359":-0.035,"360":0.166,"361":0.637,"362":0.289,"363":null,"364":0.798,"365":0.412,"366":0.032,"367":-0.016,"368":0.106,"369":0.857,"370":0.698,"371":0.154,"372":0.692,"373":0.198,"374":0.779,"375":0.842,"376":0.629,"377":-0.136,"378":0.511,"379":0.835,"380":0.689,"381":-0.051,"382":0.835,"383":-0.068,"384":0.655,"385":0.751,"386":0.387,"387":0.192,"388":0.452,"389":0.472,"390":0.404,"391":0.059,"392":0.401,"393":0.572,"394":-0.139,"395":0.764,"396":0.774,"397":0.745,"398":0.131,"399":-0.152,"400":-0.002,"401":0.673,"402":0.047,"403":0.457,"404":-0.31,"405":-0.157,"406":-0.064,"407":-0.215,"408":-0.105,"409":0.3,"410":-0.243,"411":-0.417,"412":-0.33,"413":-0.181,"414":-0.095,"415":-0.381,"416":0.458,"417":-0.026,"418":-0.112,"419":-0.024,"420":0.455,"421":0.08,"422":0.578,"423":-0.382,"424":0.236,"425":0.032,"426":0.976,"427":0.031,"428":0.573,"429":0.712,"430":0.734,"431":0.519,"432":-0.396,"433":0.008,"434":-0.083,"435":0.469,"436":0.47,"437":-0.189,"438":-0.497,"439":0.452,"440":0.014,"441":-0.224,"442":-0.422,"443":-0.111,"444":-0.268,"445":-0.347,"446":0.018,"447":0.0,"448":-0.06,"449":0.007,"450":-0.546,"451":-0.061,"452":-0.244,"453":-0.231,"454":-0.072,"455":-0.041,"456":0.187,"457":-0.112,"458":-0.1,"459":-0.244},"nominate_dim2":{"345":0.874,"346":-0.182,"347":0.162,"348":-0.04,"349":0.631,"350":-0.249,"351":0.329,"352":0.846,"353":0.328,"354":-0.889,"355":-0.373,"356":-0.186,"357":-0.325,"358":-0.343,"359":-0.463,"360":0.132,"361":-0.752,"362":-0.915,"363":null,"364":-0.27,"365":0.185,"366":0.169,"367":0.129,"368":-0.299,"369":0.223,"370":-0.202,"371":-0.123,"372":0.036,"373":0.877,"374":0.057,"375":0.37,"376":0.45,"377":-0.881,"378":0.429,"379":0.345,"380":0.215,"381":0.917,"382":0.502,"383":0.063,"384":-0.178,"385":0.401,"386":0.055,"387":0.206,"388":-0.346,"389":0.158,"390":0.913,"391":-0.355,"392":0.916,"393":0.143,"394":0.205,"395":-0.305,"396":0.258,"397":-0.004,"398":0.991,"399":-0.04,"400":0.523,"401":0.093,"402":-0.305,"403":0.351,"404":-0.042,"405":0.988,"406":-0.722,"407":-0.22,"408":0.346,"409":-0.05,"410":0.409,"411":0.909,"412":-0.944,"413":0.983,"414":0.917,"415":0.202,"416":-0.582,"417":0.483,"418":-0.105,"419":0.203,"420":-0.245,"421":-0.079,"422":0.185,"423":-0.905,"424":0.437,"425":0.181,"426":-0.102,"427":-0.406,"428":0.093,"429":0.004,"430":-0.592,"431":-0.069,"432":0.015,"433":0.948,"434":0.997,"435":-0.065,"436":0.5,"437":0.181,"438":0.868,"439":0.768,"440":-0.983,"441":-0.064,"442":0.003,"443":0.08,"444":-0.153,"445":-0.034,"446":0.068,"447":0.215,"448":-0.185,"449":-0.927,"450":0.337,"451":-0.444,"452":-0.45,"453":0.057,"454":-0.246,"455":-0.032,"456":0.092,"457":-0.693,"458":-0.035,"459":0.068},"cast_code":{"345":1,"346":1,"347":9,"348":6,"349":6,"350":"did not vote","351":9,"352":1,"353":"did not vote","354":6,"355":1,"356":1,"357":1,"358":1,"359":1,"360":"did not vote","361":6,"362":1,"363":"did not vote","364":6,"365":6,"366":1,"367":1,"368":9,"369":6,"370":6,"371":6,"372":6,"373":1,"374":"did not vote","375":9,"376":6,"377":1,"378":1,"379":"did not vote","380":6,"381":9,"382":1,"383":1,"384":6,"385":6,"386":9,"387":6,"388":1,"389":9,"390":1,"391":1,"392":1,"393":1,"394":1,"395":9,"396":6,"397":6,"398":1,"399":1,"400":9,"401":6,"402":9,"403":6,"404":1,"405":1,"406":1,"407":1,"408":1,"409":1,"410":1,"411":1,"412":1,"413":1,"414":"did not vote","415":9,"416":9,"417":1,"418":1,"419":1,"420":6,"421":"did not vote","422":6,"423":1,"424":9,"425":1,"426":6,"427":1,"428":1,"429":"did not vote","430":6,"431":9,"432":9,"433":9,"434":1,"435":6,"436":6,"437":1,"438":1,"439":1,"440":1,"441":9,"442":1,"443":1,"444":1,"445":1,"446":1,"447":1,"448":1,"449":9,"450":1,"451":9,"452":1,"453":1,"454":1,"455":6,"456":1,"457":9,"458":1,"459":1}}